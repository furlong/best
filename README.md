# best

Simple implementation of Bayesian hypothesis testing, taken from https://docs.pymc.io/notebooks/BEST.html, which in turn comes from Kruschke JK. Bayesian estimation supersedes the t test. J Exp Psychol Gen. 2013;142(2):573-603. doi:10.1037/a0029146
