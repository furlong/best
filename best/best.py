
import numpy as np

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import logging
logging.basicConfig(level=logging.ERROR)

import pymc3 as pm


def unpaired(a, b, hdi=0.95, num_samples=2000):
    '''
    unpaired hypothesis test.
    Checks to see if the difference in the means and standard deviations is different from zero.

    '''

    vals = np.concatenate((a.flatten(), b.flatten()))

    μ_m = np.mean(vals)
    μ_s = np.std(vals) * 2
    σ_low = 1
    σ_high = 10
    
    with pm.Model() as model:
        a_mean = pm.Normal("a_mean", mu=μ_m, sd=μ_s)
        b_mean = pm.Normal("b_mean", mu=μ_m, sd=μ_s)
    
        a_std = pm.Uniform("a_std", lower=σ_low, upper=σ_high)
        b_std = pm.Uniform("b_std", lower=σ_low, upper=σ_high)
    
        ν = pm.Exponential("ν_minus_one", 1 / 29.0) + 1
    
        λ1 = a_std ** -2
        λ2 = b_std ** -2
    
        a_dist = pm.StudentT("a_dist", nu=ν, mu=a_mean, lam=λ1, observed=a)
        b_dist = pm.StudentT("b_dist", nu=ν, mu=b_mean, lam=λ2, observed=b)
    
        diff_of_means = pm.Deterministic("difference of means", a_mean - b_mean)
        diff_of_stds = pm.Deterministic("difference of stds", a_std - b_std)
        effect_size = pm.Deterministic(
            "effect size", diff_of_means / np.sqrt((a_std ** 2 + b_std ** 2) / 2)
        )
   
        # TODO: Will need to be changed with pymc version 4 comes out
        trace = pm.sample(num_samples, progressbar=False, return_inferencedata=False)
    ### end with
    hdi = pm.hdi(trace, var_names=['a_mean', 'a_std', 'b_mean', 'b_std', 'difference of means', 'difference of stds', 'effect size'], hdi_prob=hdi)
    results = {
            'a':{
                'mean':{'mu':np.mean(trace['a_mean']), 'hdi':hdi['a_mean'].values},
                'std':{'mu':np.mean(trace['a_std']), 'hdi':hdi['a_std'].values},
                },
            'b':{
                'mean':{'mu':np.mean(trace['b_mean']), 'hdi':hdi['b_mean'].values},
                'std':{'mu':np.mean(trace['b_std']), 'hdi':hdi['b_std'].values},
                },
            'difference of means':{'mean':np.mean(trace['difference of means']),
                                   'hdi':hdi['difference of means'].values},
            'difference of stds':{'mean':np.mean(trace['difference of stds']),
                                  'hdi':hdi['difference of stds'].values},
            'effect size':{'mean':np.mean(trace['effect size']),
                           'hdi':hdi['effect size'].values},
           }
    return results

def paired(a, b, hdi=0.95, num_samples=2000):
    raise NotImplementedError('Forthcoming')

if __name__ == '__main__':
    drug = np.array((101,100,102,104,102,97,105,105,98,101,100,123,105,103,100,95,102,106, 109,102,82,102,100,102,102,101,102,102,103,103,97,97,103,101,97,104, 96,103,124,101,101,100,101,101,104,100,101))
    placebo = np.array((99,101,100,101,102,100,97,101,104,101,102,102,100,105,88,101,100, 104,100,100,100,101,102,103,97,101,101,100,101,99,101,100,100, 101,100,99,101,100,102,99,100,99))

    print(unpaired(drug, placebo))


