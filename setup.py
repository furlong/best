from setuptools import setup, find_packages

setup(
        name='best',
        version='0.1',
        author='P. Michael Furlong',
        author_email='michael.furlong@uwaterloo.ca',
        packages=['best'],
        install_requires=[
            'pymc3',
            'numpy'
            ]
    )
